import React from 'react'
import ReactDOM from 'react-dom'

import Example from './components/Example'

export const Here = () => {
    return (
        <div>
            <Example />
        </div>
    )
}

if (document.getElementById('app')) {
    ReactDOM.render(<Here />, document.getElementById('app'));
}
